package si.trjanci.srvolumeapp.model.events;

@SuppressWarnings("unused")
public class VolumeEvent {
    private int volume;

    public VolumeEvent(int volume) {
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
