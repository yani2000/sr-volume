package si.trjanci.srvolumeapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import si.trjanci.srvolumeapp.model.events.VolumeEvent;

public class AudioReceiver extends BroadcastReceiver {
    public static final String VOLUME_ACTION = "android.media.VOLUME_CHANGED_ACTION";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent !=null) {
            String action = intent.getAction();
            if (action != null && action.equals(VOLUME_ACTION)) {
                AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                float maxVolume = audio.getStreamMaxVolume(AudioManager.STREAM_RING);
                float volume = audio.getStreamVolume(AudioManager.STREAM_RING);
                int percent = (int)((volume / maxVolume) * 100);
                Log.d("AudioReceiver", "onReceive: MAX: "+ String.valueOf(maxVolume) + " Current: " + String.valueOf(volume));
                EventBus.getDefault().post(new VolumeEvent(percent));
            }
        }
    }
}
