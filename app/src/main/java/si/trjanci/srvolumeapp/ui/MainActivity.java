package si.trjanci.srvolumeapp.ui;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

import si.trjanci.srvolumeapp.R;
import si.trjanci.srvolumeapp.model.events.VolumeEvent;
import si.trjanci.srvolumeapp.ui.elements.VolumeIndicatorView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private VolumeIndicatorView volumeIndicatorView;
    private TextView tvVolumeSet;
    private EditText etVolume;
    private EditText etLines;

    private AudioManager audio;
    private float maxVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        maxVolume = audio.getStreamMaxVolume(AudioManager.STREAM_RING);
        float volume = audio.getStreamVolume(AudioManager.STREAM_RING);
        int percent = (int)((volume / maxVolume) * 100);

        volumeIndicatorView = findViewById(R.id.viv_main_indicator);
        tvVolumeSet = findViewById(R.id.tv_main_volumeset);
        etVolume = findViewById(R.id.et_main_volume);
        etLines = findViewById(R.id.et_main_lines);
        Button buttonVolume = findViewById(R.id.button_main_volume);
        Button buttonLines = findViewById(R.id.button_main_lines);

        buttonVolume.setOnClickListener(this);
        buttonLines.setOnClickListener(this);

        etVolume.setText(String.valueOf(percent));
        setTextVolume(percent);
        etLines.setText(String.valueOf(volumeIndicatorView.getLines()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVolumeChanged(VolumeEvent event) {
        if (event != null) {
            volumeIndicatorView.setVolume(event.getVolume());
            etVolume.setText(String.valueOf(event.getVolume()));
            setTextVolume(event.getVolume());
        }
    }

    private void setTextVolume(int vol) {
        tvVolumeSet.setText(String.format(Locale.getDefault(),
                getString(R.string.volume_set_at), vol));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_main_volume:
                String volumeText = etVolume.getText().toString();
                int vol;
                try {
                    vol = Integer.parseInt(volumeText);
                }
                catch (Exception ignore) {
                    vol = 0;
                }
                volumeIndicatorView.setVolume(vol);
                setTextVolume(vol);
                audio.setStreamVolume(AudioManager.STREAM_RING,
                        (int) (maxVolume * ((float)vol / 100)), AudioManager.FLAG_SHOW_UI);
                break;
            case R.id.button_main_lines:
                String linesText = etLines.getText().toString();
                int lines;
                try {
                    lines = Integer.parseInt(linesText);
                }
                catch (Exception ignore) {
                    lines = 0;
                }
                volumeIndicatorView.setLines(lines);
                break;
            default:
                break;
        }
    }
}
