package si.trjanci.srvolumeapp.ui.elements;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import si.trjanci.srvolumeapp.R;

public class VolumeIndicatorView extends View {
    public static final int DEFAULT_INDICATORS = 10;
    public static final int DEFAULT_VOLUME = 50;

    private int lines = DEFAULT_INDICATORS;
    private int selectedIndicators = 0;
    private int volume = DEFAULT_VOLUME;
    private int indicatorHeight = 0;

    private int defaultColor = 0;
    private int indicatorColor = 0;
    private int width = 0;
    private Paint paint;

    private int top;
    private int i;

    public VolumeIndicatorView(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public VolumeIndicatorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public VolumeIndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    public VolumeIndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        indicatorHeight = context.getResources().getDimensionPixelSize(R.dimen.main_indicator_height);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                    R.styleable.VolumeIndicatorView, defStyleAttr, defStyleRes);
            try {
                setLines(a.getInt(R.styleable.VolumeIndicatorView_lines, DEFAULT_INDICATORS));
                setDefaultColor(a.getColor(R.styleable.VolumeIndicatorView_defaultColor,
                        ContextCompat.getColor(context, R.color.gray)));
                setIndicatorColor(a.getColor(R.styleable.VolumeIndicatorView_indicatorColor,
                        ContextCompat.getColor(context, R.color.blue)));
                setVolume(a.getInt(R.styleable.VolumeIndicatorView_volume, DEFAULT_VOLUME));
            } finally {
                a.recycle();
            }
        } else {
            setLines(DEFAULT_INDICATORS);
            setDefaultColor(ContextCompat.getColor(context, R.color.gray));
            setIndicatorColor(ContextCompat.getColor(context, R.color.blue));
            setVolume(DEFAULT_VOLUME);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);

        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int desiredHeight = indicatorHeight * lines * 2;

        int width;
        int height;

        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = widthSize;
        } else {
            width = widthSize;
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }

        if (width < 0) {
            width = 0;
        }

        if (height < 0) {
            height = 0;
        }

        this.width = width;

        //Logger.printError(TAG, "Measure (width, height): "+width+", "+height);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //Log.d("VolumeIndicatorView", "Lines: " + lines + ", selectedIndicators: " + selectedIndicators + ", indicatorHeight: " + indicatorHeight + ", width: " + width);
        top = 0;
        for (i = 0; i < lines; i++) {
            paint.setColor((lines - selectedIndicators > i) ? defaultColor : indicatorColor);
            canvas.drawRect(0, top, width, top + indicatorHeight, paint);
            top += indicatorHeight * 2;
        }
    }

    public int getLines() {
        return lines;
    }

    public void setLines(int lines) {
        this.lines = lines;
        requestLayout();
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
        this.selectedIndicators = (int)(lines * ((float)volume / 100));
        invalidate();
    }

    @ColorInt
    public int getDefaultColor() {
        return defaultColor;
    }

    public void setDefaultColor(@ColorInt int defaultColor) {
        this.defaultColor = defaultColor;
        invalidate();
    }

    @ColorInt
    public int getIndicatorColor() {
        return indicatorColor;
    }

    public void setIndicatorColor(@ColorInt int indicatorColor) {
        this.indicatorColor = indicatorColor;
        invalidate();
    }

}
